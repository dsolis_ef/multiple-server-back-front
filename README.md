# Multiple Servers en un solo proyecto

Proyecto para implementar ambos lados de un sitio, tanto el back como el front, para invocarse desde diferentes puertos.

# Instalación

```git clone repo```

```cd folder_created ```

```npm install```

```npm start```

# Ejecución

Ejecutar en el browser las ligas tanto del back como front:

* http://localhost:3000
* http://localhost:3001

